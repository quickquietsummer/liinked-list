<?php
require_once "vendor/autoload.php";

use App\LinkedList\SingleLinkedList;

$linkedList = new SingleLinkedList("he");

$linkedList
    ->add('el')
    ->add('lo')
    ->add('wo')
    ->add('d')
    ->insert('wo', 'rl')
    ->insert('wo', 'rl')
    ->insert('rl', 'rl')
    ->insert('rl', 'rl')
    ->insert('rl', 'rl')
    ->insert('rl', 'wo')
    ->insert('rl', 'wo')
    ->dump()
    ->refresh('R')
    ->add('U')
    ->add('S')
    ->add('L')
    ->add('A')
    ->add('N')
    ->reverse()
    ->dump();


