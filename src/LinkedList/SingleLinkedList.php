<?php

namespace App\LinkedList;

class SingleLinkedList
{
    private Node $header;

    public function __construct(string $value)
    {
        $this->header = new Node($value);
    }

    public function find(string $value): ?Node
    {
        $current = $this->header;
        while ($current->value !== $value && $current->next !== null) {
            $current = $current->next;
        }
        return $current;
    }

    public function refresh(string $value): static
    {
        $this->header = new Node($value);
        return $this;
    }

    public function insert(string $before, string $new): static
    {
        $beforeNode = $this->find($before);
        if ($beforeNode === null) {
            throw new NotFoundException();
        }
        $newNode = new Node($new);
        $afterNode = $beforeNode->next;
        $beforeNode->next = $newNode;
        if ($afterNode !== null) {
            $newNode->next = $afterNode;
        }
        return $this;
    }

    private function last(): Node
    {
        $candidate = $this->header;
        while ($candidate->next !== null) {
            $candidate = $candidate->next;
        }
        return $candidate;
    }

    public function add(string $value): static
    {
        $last = $this->last();
        $last->next = new Node($value);
        return $this;
    }

    public function dump(): static
    {
        $tempCurrent = $this->header;
        while ($tempCurrent !== null) {
            echo $tempCurrent->value;
            if ($tempCurrent->next === null) {
                break;
            }
            echo '->';
            $tempCurrent = $tempCurrent->next;
        }
        echo "\n";
        return $this;
    }

    public function reverse(): static
    {
        $tempBefore = $this->header;
        $tempAfter = $tempBefore->next;
        $this->header = $tempAfter;
        while ($tempAfter !== null) {
            $tempBefore = $this->swap($tempBefore, $tempAfter);
            $tempAfter = $tempBefore->next;
        }
        return $this;
    }

    /**
     * Меняет местами рекурсивно и в итоге возвращает новый header
     * @param Node $node
     * @return Node
     */
    private function swap(Node $node): Node
    {
        $next = &$node->next;
        $tempFurthest = &$next->next;
        $next->next = &$node;

        /** Теперь предметно это after */
        return $node;
    }
}