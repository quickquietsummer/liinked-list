<?php

namespace App\LinkedList;

class Node
{
    public string $value;
    public ?Node $next;

    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
        $this->next = null;
    }
}